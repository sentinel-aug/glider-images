# Glider Images

Repository to store glider images that would be stored locally on an autonomy payload onboard a Slocum Glider.


## Guide

Glider Images will look to incorporate the versions of each respective package based on the commit hash or tag listed in the .env file.

As of 08/24/2021, the field guide procedure is as follows:
- Clone Glider Images onto PI4
- Run ./make-dockerfiles.sh
- docker-compose build
- docker-compose up

Note: If you run docker-compose down before powering off the backseat driver, then the docker containers will not start up automatically when the backseat driver is powered on. Bottomline: turn the PI off after running command "docker-compose up" and while containers are open.

Note: In order for micron dockerfile to work. Micron dockerfile will copy private rsa key "gitalb_key" which should be in same repo as docker-compose.yml on local computer. Matching public key pair should then be added to gitlab account with access to micron sonar repo
