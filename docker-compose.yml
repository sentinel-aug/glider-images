version: "3"

services:
  ros-master:
    image: ros:noetic-ros-base
    command: stdbuf -o L roscore
    restart: always
  
  micron:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/micron-sonar:$MICRON_REF
    build:
      context: .
      dockerfile: Dockerfile.micron
      args: &args
        battery_logger_ref: "${BATTERY_LOGGER_REF}"
        ctd_ref: "${CTD_REF}"
        slocum_glider_ref: "${SLOCUM_GLIDER_REF}"
        ds_base_ref: "${DS_BASE_REF}"
        ds_msgs_ref: "${DS_MSGS_REF}"
        ds_sensors_ref: "${DS_SENSORS_REF}"
        micron_ref: "${MICRON_REF}"
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
      - "PYTHONUNBUFFERED=1"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/micron:/home/ros/.ros"
    devices:
      - "${MICRON_SERIAL_PORT:-/dev/ttyAMA0}:/dev/ttyAMA0"
    restart: always

  extctl-serial:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/extctl-serial:$SLOCUM_GLIDER_REF
    build:
      context: .
      dockerfile: Dockerfile.extctl-serial
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/extctl-serial:/home/ros/.ros"
    devices:
      - "${SCI_SERIAL_PORT:-/dev/ttyUSB0}:/dev/tty_sci"
    restart: always
    
  pwrctl:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/pwrctl:$SLOCUM_GLIDER_REF
    build:
      context: .
      dockerfile: Dockerfile.pwrctl
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/pwrctl:/home/ros/.ros"
    restart: always
    devices:
      - "/dev/gpiomem:/dev/gpiomem"

  extctl-logger:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/extctl-logger:$SLOCUM_GLIDER_REF
    build:
      context: .
      dockerfile: Dockerfile.extctl-logger
      args: *args
    depends_on:
      - ros-master
      - extctl-serial
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
      - "PYTHONUNBUFFERED=1"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/extctl-logger:/home/ros/.ros"
    restart: always

  mission-controller:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/mission-controller:$SLOCUM_GLIDER_REF
    build:
      context: .
      dockerfile: Dockerfile.mission-controller
      args: *args
    depends_on:
      - ros-master
      - extctl-serial
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
      - "PYTHONUNBUFFERED=1"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/mission-controller:/home/ros/.ros"
    restart: always

  battery-logger:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/battery-logger:$BATTERY_LOGGER_REF
    build:
      context: .
      dockerfile: Dockerfile.battery-logger
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/battery-logger:/home/ros/.ros"
    devices:
      - "${BATTERY_LOGGER_SERIAL_PORT:-/dev/ttyUSB0}:/dev/tty_battery_logger"
    restart: always

  ctd:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/ctd:$CTD_REF
    build:
      context: .
      dockerfile: Dockerfile.ctd
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
      - "PYTHONUNBUFFERED=1"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/ctd:/home/ros/.ros"
    devices:
      - "${CTD_SERIAL_PORT:-/dev/ttyUSB0}:/dev/tty_ctd"
    restart: always

  sparton-m2-ahrs:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/ds-sensors:$DS_SENSORS_REF
    build:
      context: .
      dockerfile: Dockerfile.ds-sensors
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/sparton-m2-ahrs:/home/ros/.ros"
    devices:
      - "${SPARTON_SERIAL_PORT:-/dev/ttyAMA1}:/dev/tty_sparton"
    command:
      - stdbuf
      - -o
      - L
      - roslaunch
      - --wait
      - ds_sensors
      - spartonm2.launch
      - serial_port:=/dev/tty_sparton
    restart: always

  dvl:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/ds-sensors:$DS_SENSORS_REF
    build:
      context: .
      dockerfile: Dockerfile.ds-sensors
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/dvl:/home/ros/.ros"
    devices:
      - "${DVL_SERIAL_PORT:-/dev/ttyAMA2}:/dev/tty_dvl"
    command:
      - stdbuf
      - -o
      - L
      - roslaunch
      - --wait
      - ds_sensors
      - rdidvl.launch
      - serial_port:=/dev/tty_dvl
    restart: always

  rosbag:
    image: registry.gitlab.com/sentinel-aug/glider-images/${GLIDER_NAME}/rosbag:latest
    build:
      context: .
      dockerfile: Dockerfile.rosbag
      args: *args
    depends_on:
      - ros-master
    environment:
      - "ROS_MASTER_URI=http://ros-master:11311"
      - "ROS_PYTHON_LOG_CONFIG_FILE=/python_logging_${ROS_PYTHON_ROSOUT_LOG_LEVEL:-INFO}.conf"
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "./data/ros/rosbag:/home/ros/.ros"
    stop_grace_period: 30s
    restart: always
