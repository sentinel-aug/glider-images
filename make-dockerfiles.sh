#!/bin/sh

cat_noetic () {
    cat templates/Dockerfile.noetic-base "templates/Dockerfile.$1" > "Dockerfile.$1"
}

cat_melodic () {
    cat templates/Dockerfile.melodic-base "templates/Dockerfile.$1" > "Dockerfile.$1"
}

cat_noetic battery-logger
cat_noetic ctd
cat_noetic extctl-serial
cat_noetic extctl-logger
cat_noetic mission-controller
cat_noetic rosbag
cat_noetic pwrctl

cat_melodic ds-sensors
cat_melodic micron
